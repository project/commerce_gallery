Commerce Image Gallery
===============
[![Build Status](https://travis-ci.org/drupalcommerce/commerce.svg?branch=8.x-2.x)](https://travis-ci.org/drupalcommerce/commerce)

Commerce Image Gallery Provides the custom block to display the
commerce related responsive image gallery.

We can place the galery block anywhere on the site.
We can configure the
block with 'Heading Text', 'Number of images on gallery' and
'Number of image columns' fields.

We can set the 'Number of images on gallery' to 0 to display all the images
or limit the number of images on the gallery. We can also set the 'Number
of image columns' with available options(one, two, three or four).
The product name, skua and price will be displayed on image hover and
we will be redirected to the product details page on click.

Please report bugs in the [issue queue](https://www.drupal.org/project/issues/commerce_gallery?version=1.x).

[Issue Tracker](https://www.drupal.org/project/issues/commerce_gallery?version=1.x)

## Installation

Use [Composer](https://getcomposer.org/) to get Commerce + Gallery with all dependencies.

```
composer create-project drupalcommerce/project-base mysite
--stability dev --no-interaction
```
