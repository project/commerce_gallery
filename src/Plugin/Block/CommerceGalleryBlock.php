<?php

namespace Drupal\commerce_gallery\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_price\Entity\Currency;

/**
 * Provides a 'Commerce Gallery Block' Block.
 *
 * @Block(
 *   id = "commerce_gallery",
 *   admin_label = @Translation("Commerce Gallery"),
 *   category = @Translation("Commerce"),
 * )
 */
class CommerceGalleryBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $no_of_images = $config['no_of_images'] ?? 0;
    $no_of_columns = $config['no_of_columns'] ?? 4;
    $heading_text = $config['heading_text'] ?? '';

    // Get commerce related images.
    $valid_image_types = [
      'image/jpeg',
      'image/png',
    ];
    $query = $this->database()->select('file_managed', 'fm');
    $query->condition('fm.status', '1');
    $query->condition('fm.filemime', $valid_image_types, 'IN');
    $query->join('file_usage', 'fu', 'fu.fid = fm.fid');
    $query->join('commerce_product_variation_field_data', 'c', 'c.variation_id = fu.id');
    $query->condition('fu.type', 'commerce_product_variation');
    $query->fields('fm', ['fid', 'filename', 'uri']);
    $query->fields('fu', ['id']);
    $query->fields('c', [
      'title',
      'sku',
      'price__number',
      'price__currency_code',
      'product_id',
    ]);
    $query->orderBy('fm.fid', 'DESC');

    if ($no_of_images > 0) {
      $query->range(0, $no_of_images);
    }

    $results = $query->execute()->fetchAll();

    $results_data = [];

    if (!empty($results)) {
      $count_results = count($results);
      $items_per_chunk = ceil($count_results / $no_of_columns);
      $results_data = array_chunk($results, $items_per_chunk);
    }

    // Get the currency code from the active store.
    $currency_symbol = '';
    $entity_manager = $this->entityTypeManager();
    $store = $entity_manager->getStorage('commerce_store')->loadDefault();
    if ($store) {
      $currency_code_value = $store->get('default_currency')->getValue();
      if (isset($currency_code_value[0]['target_id'])) {
        $currency_code = $currency_code_value[0]['target_id'];
        $currency = Currency::load($currency_code);
        if ($currency) {
          $currency_symbol = $currency->get('symbol');
        }
      }
    }
    $related_data = [
      'currency_symbol' => $currency_symbol,
    ];

    return [
      '#theme' => 'commerce_gallery',
      '#gallery_data' => $results_data,
      '#heading_text' => $heading_text,
      '#related_data' => $related_data,
      '#attached' => [
        'library' => ['commerce_gallery/gallery'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['heading_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading Text'),
      '#description' => $this->t('Gallery heading text'),
      '#default_value' => $config['heading_text'] ?? '',
    ];
    $form['no_of_images'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of images on gallery'),
      '#description' => $this->t('Enter the number of images on the gallery. Enter 0 to display all images from the products.'),
      '#default_value' => $config['no_of_images'] ?? 0,
    ];
    $form['no_of_columns'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of image columns'),
      '#description' => $this->t('Select the number of image columns'),
      '#default_value' => $config['no_of_columns'] ?? '4',
      '#options' => [
        '1' => 'One',
        '2' => 'Two',
        '3' => $this->t('Three'),
        '4' => $this->t('Four'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    parent::blockSubmit($form, $form_state);
    $no_of_images = $form_state->getValue('no_of_images');
    $heading_text = $form_state->getValue('heading_text');
    $no_of_columns = $form_state->getValue('no_of_columns');
    if (empty($no_of_images)) {
      $no_of_images = 0;
    }

    $this->setConfigurationValue('no_of_images', $no_of_images);
    $this->setConfigurationValue('heading_text', $heading_text);
    $this->setConfigurationValue('no_of_columns', $no_of_columns);
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

  }

  /**
   * Return 0 If you want to disable caching for this block.
   */
  public function getCacheMaxAge() {

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}
